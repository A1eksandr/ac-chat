Rails.application.routes.draw do
  get 'messages/create'

  resources :messages, only: [:index, :create]
  resources :sessions, only: [:new, :create]

  root 'sessions#new'
end
